function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">
          ${starts}-${ends}
        </div>
      </div>
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const error = new Error("Response not ok");
        const main = document.querySelector('.container');
        const html = createAlert(error);
        main.innerHTML += html;
        
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const starts = new Date(details.conference.starts).toLocaleDateString()
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        const main = document.querySelector(".container");
        const error = e;
        const html = createAlert(error);
        main.innerHTML += html;
    }

  });
